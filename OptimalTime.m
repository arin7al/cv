function outStruct = OptimalTime(A, B, f, t0, tf, a, b, r0, xr0, n, pointMatr, inStruct, isGraphic)
flag1 = ~isempty(inStruct);

if (~isempty(inStruct))
    topt = inStruct.topt;
    iopt = inStruct.iopt;
    psiopt = inStruct.psiopt;
    xopt = inStruct.xopt;
    psi1ResMatr = inStruct.psi1;
    psi2ResMatr = inStruct.psi2;
    N = inStruct.N;       %������ ����� �� t
    
    if (inStruct.global == 1)   %���� �� � ������ ����������� ���������
        flag1 = 1;
        flag2 = 0;
        
        Niter = N - 1;
        N = N*2 - 1;
        ugol2 = linspace(0, 2*pi, N);
        ugol = ugol2(2:2:end);  %�� n - 1  
    else
        if (inStruct.local == 1) %���� �� � ������ ���������� ���������
            Nt = 50;
            plot(inStruct.resxM(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + inStruct.sizeM(iopt)), inStruct.resyM(Nt*(iopt - 1) + 1 : Nt*(iopt - 1) + inStruct.sizeM(iopt)),'k');
            
            flag2 = 1;
            flag1 = 0;
            deltaugol1 = inStruct.delta;
            %ugolopt = iopt*deltaugol1;
            ugolopt = inStruct.ugol;
            %N1 = N*2 - 1;
            %deltaugol2 = 2*pi/(N1-1);
            deltaugol2 = deltaugol1/2;
            ugol1 = ugolopt - deltaugol2;
            ugol2 = ugolopt + deltaugol2;
            N = N + 2;
            %ugol = [ugol1, ugol2];
            %Niter = 2;
            
            %�������� �������
            i1opt = iopt;
            
            options = odeset('Events', @(t, x) IsInX(t, x, pointMatr, n), 'RelTol',1e-8,'AbsTol',1e-10); 
            Nt = 50;
            tsetka = linspace(t0, tf, Nt);
            
            outStruct.ugol = ugolopt;
            psi0 = [cos(ugol1); sin(ugol1)];
            x0 = r0*psi0 + xr0;
            l = transpose(B)*psi0;
            if (a == 1)
                u = sqrt(b)*l/norm(l);
            else
                if (a > 1)
                    if (abs(l(2)) > abs(l(1)))
                        u = sqrt(a*b/(a*l(1)^2 + l(2)^2))*(l(1)^2 + l(2)^2/a);
                    else
                        u = sqrt(a*b/(a*l(2)^2 + l(1)^2))*(l(2)^2 + l(1)^2/a);
                    end
                else
                    if (abs(l(2)) > abs(l(1)))
                        u = sqrt(a*b/(a*l(2)^2 + l(1)^2))*(l(2)^2 + l(1)^2/a);
                    else
                        u = sqrt(a*b/(a*l(1)^2 + l(2)^2))*(l(1)^2 + l(2)^2/a);
                    end                
                end
            end
            [t, y, te, ye, ie] = ode45(@(t, x) Fun(t, x, A, B, u, f), tsetka, x0, options);
            sizey1 = size(y);
            xVect1 = y(:, 1);
            yVect1 = y(:, 2);
            plot(xVect1, yVect1, 'k');
            if (isempty(te) == 0) 
                if (te < topt)
                    topt = te;
                    xopt = [ye(1), ye(2)];
                    i1opt = iopt;
                    outStruct.ug
                    outStruct.ugol = ugol1;
                end
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
            psi0 = [cos(ugol2); sin(ugol2)];
            x0 = r0*psi0 + xr0;
            l = transpose(B)*psi0;
            if (a == 1)
                u = sqrt(b)*l/norm(l);
            else
                if (a > 1)
                    if (abs(l(2)) > abs(l(1)))
                        u = sqrt(a*b/(a*l(1)^2 + l(2)^2))*(l(1)^2 + l(2)^2/a);
                    else
                        u = sqrt(a*b/(a*l(2)^2 + l(1)^2))*(l(2)^2 + l(1)^2/a);
                    end
                else
                    if (abs(l(2)) > abs(l(1)))
                        u = sqrt(a*b/(a*l(2)^2 + l(1)^2))*(l(2)^2 + l(1)^2/a);
                    else
                        u = sqrt(a*b/(a*l(1)^2 + l(2)^2))*(l(1)^2 + l(2)^2/a);
                    end                
                end
            end
            [t, y, te, ye, ie] = ode45(@(t, x) Fun(t, x, A, B, u, f), tsetka, x0, options);
            sizey2 = size(y);
            xVect2 = y(:, 1);
            yVect2 = y(:, 2);
            plot(xVect2, yVect2, 'k');
            if (isempty(te) == 0) 
                if (te < topt)
                    topt = te;
                    xopt = [ye(1), ye(2)];
                    i1opt = iopt + 2;
                    outStruct.ugol = ugol2;
                end
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            inSizeMatr = inStruct.sizeM;
            inxResultMatr = inStruct.resxM;
            inyResultMatr = inStruct.resyM;
            
            xResMatr = zeros(Nt, N);
            yResMatr = zeros(Nt, N);
            sizeMatr = zeros(N, 1);
            
            xResMatr(1:Nt*(iopt - 1)) = inxResultMatr(1:Nt*(iopt - 1));
            xResMatr(Nt*(iopt - 1) + 1 : Nt*(iopt-1) + sizey1(1)) = xVect1;
            xResMatr(Nt*iopt + 1: Nt*(iopt + 1)) = inxResultMatr(Nt*(iopt-1) + 1:Nt*iopt);
            xResMatr(Nt*(iopt + 1) + 1 : Nt*(iopt+1) + sizey2(1)) = xVect2;
            xResMatr(Nt*(iopt + 2) + 1: end) = inxResultMatr(Nt*iopt + 1: end);
            
            yResMatr(1:Nt*(iopt - 1)) = inyResultMatr(1:Nt*(iopt - 1));
            yResMatr(Nt*(iopt - 1) + 1 : Nt*(iopt-1) + sizey1(1)) = yVect1;
            yResMatr(Nt*iopt + 1: Nt*(iopt + 1)) = inyResultMatr(Nt*(iopt-1) + 1:Nt*iopt);
            yResMatr(Nt*(iopt + 1) + 1 : Nt*(iopt+1) + sizey2(1)) = yVect2;
            yResMatr(Nt*(iopt + 2) + 1: end) = inyResultMatr(Nt*iopt + 1: end);
            
            sizeMatr(1:iopt-1) = inSizeMatr(1:iopt-1);
            sizeMatr(iopt) = sizey1(1);
            sizeMatr(iopt+1) = inSizeMatr(iopt);
            sizeMatr(iopt+2) = sizey2(1);
            sizeMatr(iopt+3: end) = inSizeMatr(iopt+1:end);
            
            iopt = i1opt;
        end
    end
else
    N = 30; %���-�� ��� 
    Niter = N;
    ugol = linspace(0, 2*pi, N);
    
    flag1 = 0;
    flag2 = 0;
    topt = 2*tf;
    iopt = -1;
    psiopt = [0;0];
    xopt = [0,0];
end

if(~flag2)
    %������ ��������� �0 � �1
    ugol1 = linspace(0, 2*pi, 100);  %�������������� ������
    x1Vec = r0*cos(ugol1) + repmat(xr0(1), 1, 100);
    y1Vec = r0*sin(ugol1) + repmat(xr0(2), 1, 100);
    figure;
    plot(x1Vec, y1Vec);
    hold on;

    plot(pointMatr(:, 1), pointMatr(:, 2));
    plot([pointMatr(n), pointMatr(1)], [pointMatr(2*n), pointMatr(n+1)]);

    deltaugol2 = 2*pi/(N-1);

    psi = [cos(ugol); sin(ugol)];  %��� ������ ���� ������

    options = odeset('Events', @(t, x) IsInX(t, x, pointMatr, n), 'RelTol',1e-8,'AbsTol',1e-10); 
    %��� ���� � options ���������� �������� ����������

    Nt = 50;
    tsetka = linspace(t0, tf, Nt);

    xResMatr = zeros(Nt, N);
    yResMatr = zeros(Nt, N);
    psi1ResMatr = zeros(Nt, N);
    psi2ResMatr = zeros(Nt, N);    
    tMatr = zeros(Nt, N);
    psitMatr = zeros(Nt, N);
    
    sizeMatr = zeros(N, 1);
    u1Matr = zeros(N, 1);
    u2Matr = zeros(N, 1);
    for i = 1:Niter
        psi0 = [psi(2*i - 1); psi(2*i)];
        
        [t,ypsi] = ode45(@(t, x) Fpsi(t, x, A),tsetka,psi0);
        psi1ResMatr(Nt*(i-1) + 1 : Nt*(i)) = ypsi(:, 1);
        psi2ResMatr(Nt*(i-1) + 1 : Nt*(i)) = ypsi(:, 2);
        psitMatr(Nt*(i-1) + 1 : Nt*(i)) = t;
        
        x0 = r0*psi0 + xr0;
        l = transpose(B)*psi0;
        %����������
        if (a == 1)
            u = sqrt(b)*l/norm(l);
        else
            if (a > 1)
                if (abs(l(2)) > abs(l(1)))
                    u = sqrt(a*b/(a*l(1)^2 + l(2)^2))*[l(1); l(2)/a];
                else
                    u = sqrt(a*b/(a*l(2)^2 + l(1)^2))*[l(1)/a; l(2)];
                end
            else
                if (abs(l(2)) > abs(l(1)))
                    u = sqrt(a*b/(a*l(2)^2 + l(1)^2))*[l(1)/a; l(2)];
                else
                    u = sqrt(a*b/(a*l(1)^2 + l(2)^2))*[l(1); l(2)/a];
                end                
            end
        end
        u1Matr(i) = u(1);
        u2Matr(i) = u(2);
        
        [t, y, te, ye, ie] = ode45(@(t, x) Fun(t, x, A, B, u, f), tsetka, x0, options);
        sizey = size(y);

        if (isempty(te) == 0) 
            if (te < topt)
                topt = te;
                if flag1
                    iopt = 2*i - 1;
                else
                    iopt = i;
                end
                psiopt = [psi1ResMatr(Nt*(i-1) +sizey(1)); psi2ResMatr(Nt*(i-1) +sizey(1))];
                xopt = [ye(1), ye(2)];
            end
        end
        xVect = y(:, 1);
        yVect = y(:, 2);
        if flag1             %���� �� � ������ ����������� ���������
            inSizeMatr = inStruct.sizeM;
            inxResultMatr = inStruct.resxM;
            inyResultMatr = inStruct.resyM;

            xResMatr((Nt*2*(i-1) + 1):(Nt*2*(i-1) + inSizeMatr(i))) = inxResultMatr((Nt*(i-1) + 1):(Nt*(i-1) + inSizeMatr(i)));
            yResMatr((Nt*2*(i-1) + 1):(Nt*2*(i-1) + inSizeMatr(i))) = inyResultMatr((Nt*(i-1) + 1):(Nt*(i-1) + inSizeMatr(i)));
            xResMatr((Nt*2*(i-1) + Nt + 1):(Nt*2*(i-1) + Nt + sizey(1))) = xVect;
            yResMatr((Nt*2*(i-1) + Nt + 1):(Nt*2*(i-1) + Nt + sizey(1))) = yVect;

            %size1 = min(inSizeMatr(i), sizey(1));
            %size2 = size1;
            sizeMatr(2*i - 1) = inSizeMatr(i);
            sizeMatr(2*i) = sizey(1);

            %x1Vect = zeros(size1 + size2, 1);
            %x1Vect(1:2:end) = xVect(1:size1);
            %x1Vect(2:2:end) = inResultMatr(1:size2, 2*i - 1);

            %y1Vect = zeros(size1 + size2, 1);
            %y1Vect(1:2:end) = yVect(1:size1);
            %y1Vect(2:2:end) = inResultMatr(1:size2, 2*i);

            %resultMatr((Nt*2*(i-1) +1):(Nt*2*(i-1) +size1 + size2)) = x1Vect;
            %resultMatr((Nt*2*(i-1) + Nt +1):(Nt*2*(i-1)+ Nt +size1 + size2)) = y1Vect;
            plot(inxResultMatr((Nt*(i-1) + 1):(Nt*(i-1) + inSizeMatr(i))), inyResultMatr((Nt*(i-1) + 1):(Nt*(i-1) + inSizeMatr(i))), 'k');
            plot(xVect, yVect, 'k');
        else
            if flag2        %���� �� � ������ ���������� ���������
                
                plot(xVect, yVect);
                
            else                %���� �� ��� �������
                sizeMatr(i) = sizey(1);
                xResMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizey(1)) = xVect;
                yResMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizey(1)) = yVect;
                tMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizey(1)) = t;
                
                plot(xVect, yVect, 'k');
            end
        end
    end

    xlim([-2;5]);
    ylim([-1.5;2.5]);
    xlabel('x1');
    ylabel('x2');
    %title('trajectories');
end

%������ ����������� ���������� �������
if (iopt>0)
    plot(xResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), yResMatr(Nt*(iopt - 1) + 1 : Nt*(iopt - 1) + sizeMatr(iopt)),'r');
end
%������ ���
%quiver(xopt(1), xopt(2), psiopt(1)/sqrt(psiopt(1)^2 + psiopt(2)^2)/2,  -psiopt(2)/sqrt(psiopt(1)^2 + psiopt(2)^2)/2);


%������ ������� � �����
%������ � ����� ����� ���������
pointM = repmat([xopt(1), xopt(2)], n, 1);
helpM = pointM - pointMatr;
helpM= helpM.^2;
helpM1 = sum(helpM, 2);
[M, I1] = min(helpM1);
helpM1(I1) = 100;
[M, I2] = min(helpM1);          %���� ����� - ����� I1 � I2

Imax = max(I1, I2);
Imin = I1 + I2 - Imax;

    pointMatr1 = circshift(pointMatr, 1, 1);    % ����� �� 1 ���� (n. 1. 2. )
    razMatr = pointMatr - pointMatr1;           % 1-n... 2-1.. 3-2..
    pointMatr2 = circshift(pointMatr, -1, 1);    % ����� �� 1 ����� (2. 3. ...)
    pointMatr2 = circshift(pointMatr2, 1, 2);   % ������ ������� x � y 
    pointMatr1 = circshift(pointMatr1, 1,2);    % ������ ������� x � y 
    sigmaVect = -diff(razMatr.*pointMatr2, 1, 2) + diff(razMatr.*pointMatr1, 1, 2);        %���� ���������� �������

ax = pointMatr(Imax) - pointMatr(Imin);
ay = pointMatr(n + Imax) - pointMatr(n + Imin);
%quiver(xopt(1), xopt(2), -ay/sqrt(ax^2 + ay^2)/2, ax/sqrt(ax^2 + ay^2)/2);



outStruct.sizeM = sizeMatr;
outStruct.resxM = xResMatr;
outStruct.resyM = yResMatr;
outStruct.psi1 = psi1ResMatr;
outStruct.psi2 = psi2ResMatr;
outStruct.N = N;
outStruct.iopt = iopt;
outStruct.topt = topt;
outStruct.psiopt = psiopt;
outStruct.xopt = xopt;

%k = input('Do you want to build only optimal trajectories?');
k = 0;
if (k==1)
    figure;
    %������ ��������� �0 � �1
    ugol1 = linspace(0, 2*pi, 100);  %�������������� ������
    x1Vec = r0*cos(ugol1);
    y1Vec = r0*sin(ugol1);
    plot(x1Vec, y1Vec);
    hold on;
    plot(pointMatr(:, 1), pointMatr(:, 2));
    plot([pointMatr(n), pointMatr(1)], [pointMatr(2*n), pointMatr(n+1)]);
    
    plot(xResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), yResMatr(Nt*(iopt - 1) + 1 : Nt*(iopt - 1) + sizeMatr(iopt)),'r');
    xlim([-2;4]);
    ylim([-1;5]);
    xlabel('x1');
    ylabel('x2');
    
    figure; %�1 �� t
    plot(tMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)),xResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), 'r');
    xlabel('t');
    ylabel('x1');
    
    figure; %x2 �� t
    plot(tMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)),yResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), 'r');
    xlabel('t');
    ylabel('x2');
    
    figure; %psi1 �� t
    plot(tMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)),psi1ResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), 'r');
    xlabel('t');
    ylabel('psi1');
    
    figure; %psi2 �� t
    plot(tMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)),psi2ResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), 'r');
    xlabel('t');
    ylabel('psi2');
    
     figure; % u1 t
    plot([t0, tMatr(Nt*(iopt-1) + sizeMatr(iopt))], [u1Matr(iopt), u1Matr(iopt)], 'r');
    xlabel('t');
    ylabel('u1');
    
    figure; % u2 t
    plot([t0, tMatr(Nt*(iopt-1) + sizeMatr(iopt))], [u2Matr(iopt), u2Matr(iopt)], 'r');
    xlabel('t');
    ylabel('u2');
    
    figure; % u2 u1
    ugol1 = linspace(0, 2*pi, 100);  %�������������� ������
    x1Vec = r0*cos(ugol1) + repmat(xr0(1), 1, 100);
    y1Vec = r0*sin(ugol1) + repmat(xr0(2), 1, 100);
    plot(x1Vec, y1Vec);
    hold on;
    plot(u1Matr(iopt), u2Matr(iopt), 'r*');
    xlabel('u1');
    ylabel('u2');
    
end
    
if (isGraphic)                  %����� ���������
    figure;
    hold on;
    for i=1:N                   %�1 �� t
        plot(tMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizeMatr(i)), xResMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizeMatr(i)), 'k');
    end
    %������ ����������� ���������� �������
    
    plot(tMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)),xResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), 'r');
    xlabel('t');
    ylabel('x1');
    
    figure;
    hold on;
    for i=1:N                   %x2 �� t
        plot(tMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizeMatr(i)), yResMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizeMatr(i)), 'k');
    end
    %������ ����������� ���������� �������
    plot(tMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)),yResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), 'r');
   
    xlabel('t');
    ylabel('x2');
    
    figure;
    hold on;
    for i=1:N                   %psi1 �� t
        plot(psitMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizeMatr(i)), psi1ResMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizeMatr(i)), 'k');
    end
    %������ ����������� ���������� �������
    plot(tMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)),psi1ResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), 'r');
   
    xlabel('t');
    ylabel('psi1');
    
    figure;
    hold on;
    for i=1:N                   %psi2 �� t
        plot(psitMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizeMatr(i)), psi2ResMatr(Nt*(i-1) + 1 : Nt*(i-1) + sizeMatr(i)), 'k');
    end
    %������ ����������� ���������� �������
    plot(tMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)),psi2ResMatr(Nt*(iopt - 1) + 1: Nt*(iopt - 1) + sizeMatr(iopt)), 'r');
   
    xlabel('t');
    ylabel('psi2');
    
    figure;
    hold on;
    for i=1:N
        plot([t0, tMatr(Nt*(i-1) + sizeMatr(i))], [u1Matr(i), u1Matr(i)], 'k');    %u1 t
    end
    %������ ����������� ���������� �������
    plot([t0, tMatr(Nt*(iopt-1) + sizeMatr(iopt))], [u1Matr(iopt), u1Matr(iopt)], 'r');   
    
    xlabel('t');
    ylabel('u1');
    
    figure;
    hold on;
    for i=1:N
        plot([t0, tMatr(Nt*(i-1) + sizeMatr(i))], [u2Matr(i), u2Matr(i)], 'k');    %u2 t
    end
    %������ ����������� ���������� �������
    plot([t0, tMatr(Nt*(iopt-1) + sizeMatr(iopt))], [u2Matr(iopt), u2Matr(iopt)], 'r');
    
    xlabel('t');
    ylabel('u2');
    
    figure;
    hold on;
    for i=1:N
        plot(u1Matr(i), u2Matr(i), 'k*');    %u2 u1
    end
    plot(u1Matr(iopt), u2Matr(iopt), 'r*');
    xlabel('u1');
    ylabel('u2');
    
end

if (topt == 2*tf)
    disp('Not solvable');
    k = input('Do you want to try with a different t1?');
    %%%%%%%%%%%%%%%%
else
    disp('Optimal time is');
    disp(topt);    
    
    err = xopt*psiopt/(norm(xopt)*norm(psiopt));
    disp('Transversality error is');
    disp(1 - err);   % 1 - cos(a)
    
    k = input('Do you want to make it more percise globally?');
    if (k==1)
        outStruct.global = 1;
        outStruct.local = 0;
        OptimalTime(A, B, f, t0, tf, a, b, r0, xr0, n, pointMatr, outStruct, 0);
    else
        k = input('Do you want to make it more percise locally?');
        if (k==1)
            outStruct.global = 0;
            outStruct.local = 1;
            outStruct.delta = deltaugol2;
            if (~flag2)
                outStruct.ugol = (iopt-1)*deltaugol2;
            end
            OptimalTime(A, B, f, t0, tf, a, b, r0, xr0, n, pointMatr, outStruct, 0);
        end
        
    end
    
end
end



