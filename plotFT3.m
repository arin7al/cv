function [ mystruct ] = plotFT3( hFigure, fHandle, fFTHandle, step, inpLimVec, outLimVec )
    a = inpLimVec(1);
    b = inpLimVec(2);
    
    %changing step
    N = floor((inpLimVec(2) - inpLimVec(1))/step);
    step = (inpLimVec(2) - inpLimVec(1))/(N);
    tVec = linspace(inpLimVec(1), inpLimVec(2), N+1);
    tVec = tVec(1:N);
    xVec = fHandle(tVec);
        
    %defining output characteristics
    step1 = (2*pi)/(b-a);
    d = (pi)/step;
    c = -d;
    lVec = linspace(c, d, N);
    
    K = ceil(a/step);
    xVec1 = circshift(xVec, [0,K] );
    yVec = fft(xVec1);
    yVec1 = fftshift(yVec);   
    
    if (isempty(outLimVec))         %%��� ��������?
        c1 = -10;
        d1 = 10;
    else
        c1 = outLimVec(1);
        d1 = outLimVec(2);
    end              
    if(c1 > c)
        K1 = ceil((c1 - c)/step1);
    else
        K1 = 1;
    end
    if (d1 < d)
        K2 = floor((d1 - c)/step1);
    else
        K2 = N;
    end
    

    
    %setting SPlotInfo
    SPlotInfo = get(hFigure, 'UserData');
    children = get(hFigure, 'Children');
    if (isempty(children))        
        axRe = subplot(1,2,1);
        axIm = subplot(1,2,2);
        SPlotInfo.aRe = axRe;
        SPlotInfo.aIm = axIm;   
    else
        clf();
        axRe = subplot(1,2,1);
        axIm = subplot(1,2,2);
        SPlotInfo.aRe = axRe;
        SPlotInfo.aIm = axIm;   
    end
    lVec1 = lVec(K1:K2);
    yVec11 = yVec1.*step;   %circshift(xVec, -K ); ���� *exp(-1i*a.*lVec1)
    yVec11 = yVec11(K1:K2);
    SPlotInfo.X = lVec1;
    SPlotInfo.Re = real(yVec11);
    SPlotInfo.Im = imag(yVec11);
    if (isempty(fFTHandle) ~= 1)
        SPlotInfo.FRe = real(fFTHandle(lVec(K1:K2)));
        SPlotInfo.FIm = imag(fFTHandle(lVec(K1:K2)));
    end
    
    % plotting Re
    plot(SPlotInfo.aRe, SPlotInfo.X, SPlotInfo.Re, 'DisplayName', 'fft');         
    hold(SPlotInfo.aRe, 'on');
    l1Vec = linspace(- 2*pi/(step), 2*pi/(step), N );
    if (isempty(fFTHandle) ~= 1)
        plot(SPlotInfo.aRe, SPlotInfo.X, SPlotInfo.FRe, 'DisplayName', 'analytical');
       
        plot(SPlotInfo.aRe, l1Vec, real(fFTHandle(l1Vec + 2*pi/(step))), 'green');
        plot(SPlotInfo.aRe, l1Vec, real(fFTHandle(l1Vec - 2*pi/(step))), 'green');
        legend(SPlotInfo.aRe, 'fft', 'analytical', 'shifted analytical');
    else
        legend(SPlotInfo.aRe, 'fft'); 
    end
    SPlotInfo.aRe.YLabel.String = 'Re';
    SPlotInfo.aRe.XLabel.String = '\lambda';
    hold(SPlotInfo.aRe, 'off');
    % plotting Im
    plot(SPlotInfo.aIm, SPlotInfo.X, SPlotInfo.Im, 'DisplayName', 'fft');
    hold(SPlotInfo.aIm, 'on');
    if (isempty(fFTHandle) ~= 1)
        plot(SPlotInfo.aIm, SPlotInfo.X, SPlotInfo.FIm, 'DisplayName', 'analytical');
        
        plot(SPlotInfo.aIm, l1Vec, imag(fFTHandle(l1Vec + 2*pi/(step))), 'green');
        plot(SPlotInfo.aIm, l1Vec, imag(fFTHandle(l1Vec - 2*pi/(step))), 'green');
        legend(SPlotInfo.aIm, 'fft', 'analytical', 'shifted analytical');
    else
        legend(SPlotInfo.aIm, 'fft'); 
    end
    SPlotInfo.aIm.YLabel.String = 'Im';
    SPlotInfo.aIm.XLabel.String = '\lambda';
    
    mystruct.nPoints = N;
    mystruct.step = step;
    mystruct.inpLimVec = inpLimVec;
    mystruct.outLimVec = [lVec(K1), lVec(K2)];
end

