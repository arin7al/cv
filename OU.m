%% Block1

A = [1, 2; 3, 4];
B = [1, 0; 1, 2];
f = [0; 0];
t0 = 0;
tf = 0.5;
a = 1;
b = 2;
r0 = 1;
xr0 = 0;
n = 3;
pointMatr = [2, 2; 1, 1; 3, 1]; %����������� ������ ������� �������

Nt = 50;    %������ ����� �� t

%������ ��������� �0 � �1
ugol = linspace(0, 2*pi, 100);
x1Vec = r0*cos(ugol);
y1Vec = r0*sin(ugol);
plot(x1Vec, y1Vec);
hold on;

plot(pointMatr(:, 1), pointMatr(:, 2));
plot([pointMatr(n), pointMatr(1)], [pointMatr(2*n), pointMatr(n+1)]);

N = 20; %���-�� ���
ugol = linspace(0, 2*pi, N);
psi = [cos(ugol); sin(ugol)];

options = odeset('Events', @(t, x) IsInX1(t, x, pointMatr, n)); 

topt = 2*tf;
yopt = [0, 0];
isSolvable = 0;
for i = 1:N
    psi0 = [psi(2*i - 1); psi(2*i)];
    x0 = r0*psi0;
    l = transpose(B)*psi0;
    %����������
    if (a == 1)
        u = sqrt(b)*l/norm(l);
    else
        if (a > 1)
            if (abs(l(2)) > abs(l(1)))
                u = sqrt(a*b/(a*l(1)^2 + l(2)^2))*[l(1); l(2)/a];
            else
                u = sqrt(a*b/(a*l(2)^2 + l(1)^2))*[l(1)/a; l(2)];
            end
        else
            if (abs(l(2)) > abs(l(1)))
                u = sqrt(a*b/(a*l(2)^2 + l(1)^2))*(l(2)^2 + l(1)^2/a);
            else
                u = sqrt(a*b/(a*l(1)^2 + l(2)^2))*(l(1)^2 + l(2)^2/a);
            end                
        end
    end
    [t, y, te, ye, ie] = ode45(@(t, x) Fun(t, x, A, B, u, f), [t0, tf], x0, options);
    if (isempty(te) == 0) 
        if (te < topt)
            topt = te;
            yopt = ye;
        end
    end
    xVect = y(:, 1);
    yVect = y(:, 2);
    plot(xVect, yVect);

end
xlim([-2;4]);
ylim([-3;3]);
xlabel('x1');
ylabel('x2');
title('trajectories');
if (topt == 2*tf)
    disp('Not solvable');
else
    disp('Optimal time is');
    disp(topt);    
end

%% Block2
A = [2, 1; 1, -5];
B = [3, 0; 0, 3];
f = [0; 0];
t0 = 0;
tf = 1;
a = 1;
b = 1;
r0 = 1;
xr0 = [0; 1];
n =3;
pointMatr = [1.2, 1.35; 4.5, 1; 4, 2];

OptimalTime(A, B, f, t0, tf, a, b, r0, xr0, n, pointMatr, [], 0);
